;;; early-init.el --- Ajustes de preinicio de emacs   -*- lexical-binding: t -*-

;;; Comentario

;;; Código

(setq x-gtk-resize-child-frames 'resize-mode)
(setq package-enable-at-startup t)
(setq package-quickstart t)
(setq frame-inhibit-implied-resize t)
(setq gc-cons-threshold most-positive-fixnum)
(setq gc-cons-percentage 0.6)

(when (featurep 'menu-bar)
    (push '(menu-bar-lines . 0) default-frame-alist))

(push '(tool-bar-lines . 0) default-frame-alist)
(push '(height . 40) default-frame-alist)
(push '(width  . 80) default-frame-alist)
(push '(vertical-scroll-bars . nil) default-frame-alist)
(push '(horizontal-scroll-bar . nil) default-frame-alist)
(push '(left-fringe  . 1) default-frame-alist)
(push '(right-fringe . 1) default-frame-alist)

(setq frame-title-format '("  "))
(defconst mode-line--default-format mode-line-format)
(setq mode-line-format nil)

(defconst economacs--lisp-path
  (file-name-as-directory
   (concat user-emacs-directory "lisp")))

(add-to-list 'load-path economacs--lisp-path)

(let ((nanofile "contrib/nano/nano-colors.el")
      (splashfile "contrib/rougier/splash-screen.el")
      (fmtt-list))
    (dolist (str-list '(nyx-theme faces themes) fmtt-list)
        (setq fmtt-list
	        (cons
              (format "themes/elemental-%s.el" str-list)
	           fmtt-list)))
    (mapc 'load
	    (list
	    (expand-file-name nanofile economacs--lisp-path)
	    (expand-file-name splashfile economacs--lisp-path)
	    (expand-file-name (nth 0 fmtt-list) economacs--lisp-path)
	    (expand-file-name (nth 1 fmtt-list) economacs--lisp-path)
	    (expand-file-name (nth 2 fmtt-list) economacs--lisp-path))))

(load-theme 'elemental-nyx t)
(enable-theme 'elemental-nyx)

(defun economacs-after-startup ()
  (setq gc-cons-threshold 16777216
	gc-cons-percentage 0.1))

(add-hook #'emacs-startup-hook #'economacs-after-startup)

(provide 'early-init)
;;; early-init.el termina aqui



