;;; elemental-themes.el --- Tema basado en elementary os

;;; Copyright

;;; Author

;;; Comments

;;; Code

(require 'nano-colors)

(defmacro elemental-themes-with-colors-variables (variant &rest body)
    "Helper macro to setup colors for the provided VARIANT to be used in BODY."
    (declare (indent defun))
    `(let* ((colors (cdr (assoc ,variant elemental-color-palette)))
            (main-bg-color (economacs-sysTheme-color "eJuno-dark-4"))
            (alt-bg-color (economacs-sysTheme-color "eJuno-dark-2"))                                              
            (main-hl-color (economacs-sysTheme-color "eJuno-dark-6"))
            (alt-hl-color (economacs-sysTheme-color "eJuno-dark-1"))                                           
            (main-fg-color (elemental-color "silver-1"))
            ;; Aquí van otros colores

            (class '((class color) (min-colors 89))))
        ,@body)
